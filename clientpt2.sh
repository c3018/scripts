#!/bin/bash

# Snaps
# snap install "john-the-ripper"
# snap install "cool-retro-term" --classic

echo "alias john='snap run john-the-ripper'" >> ~/.bashrc
echo "alias cool='snap run cool-retro-term'" >> ~/.bashrc

# Juice-shop
docker run -d --restart "unless-stopped" --name "juice-shop" -p 3000:3000 "santosomar/juice-shop-arm64"

# # DVVWA
# git clone "https://github.com/opsxcq/docker-vulnerable-dvwa.git"
# cd "docker-vulnerable-dvwa" && docker build -t "dvwa-arm" . 
# docker run -d --restart "unless-stopped" --name "dvwa" -p 8080:80 "dvwa-arm"

# # Cyberscape client
# git clone "https://gitlab.com/c3018/flask-cyberscape-client.git"
# cd "flassk-cyberscape-client" && docker build -t "cyberscape-client" . 
# docker run -d --restart "unless-stopped" --name "cyberscape" -p 80:5000 "cyberscape-client"

# Webssh
# cd "/opt" && git clone "https://github.com/huashengdun/webssh.git"
# cd "/opt/webssh" && docker build -t "webssh-arm" . 
# docker run -d --restart "unless-stopped" --name "webssh" -p 8888:8888 "webssh-arm"

