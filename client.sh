#!/bin/bash
apt update && apt upgrade -y
apt-get install -y \
zip \
unzip \
git \
curl \
rsync \
tmux \
whois \
code \
snapd \
netcat \
python3 \
python3-dev \
python3-pip \
python3-venv \
python-dev \
python-setuptools \
terminator \
nmap \
hydra \
steghide \
gobuster \
binwalk \
exiftool \
hexedit \
tcpdump;

# Python packages
pip3 install \
sqlmap \
requests \
pynvim

# Docker
apt install -y "apt-transport-https" "ca-certificates" "software-properties-common"
curl -fsSL "https://get.docker.com" -o "get-docker.sh" && sh "get-docker.sh"
usermod -aG docker dietpi

wget https://gitlab.com/c3018/scripts/-/raw/main/id_rsa
mkdir ~/.ssh
mv ./id_rsa ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

reboot now

# diet pi lxpolkit error fix/hack, keep for reference do not comment out
# sudo mv /usr/bin/lxpolkit /usr/bin/lxpolkit.bak
