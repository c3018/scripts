#!/bin/bash
apt update && sudo apt upgrade -y
apt-get install -y \
git \
zip \
curl \
unzip \
netcat \
python3 \
python3-dev \
python3-pip \
tmux \
rsync ;

# Ansible
python3 -m pip install ansible

# K3s
curl -sfL https://get.k3s.io | sh -
ls
sudo cat /var/lib/rancher/k3s/server/node-token # client token
sudo cat /etc/rancher/k3s/k3s.yaml # kube config

kubectl get pods --all-namespaces

# Client 
curl -sfL http://get.k3s.io | K3S_URL=https://192.168.0.50:6443 \
K3S_TOKEN=join_token_we_copied_earlier sh -


# Docker
apt install -y apt-transport-https ca-certificates software-properties-common
curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh

# Registry
docker pull registry



# Flask
git clone 
